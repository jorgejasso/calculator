Feature: Basic operations of a calculator
  Scenario Outline: Operations
    Given Num <f_num> operation "<ope>" num <s_num> 
    When Calculator is run
    Then The output should be "<output>"
    Examples:
      | f_num | ope | s_num | output |
      | 2     | +   | 2     | 4      |
      | 5     | +   | 4     | 9      |
      | 8     | +   | 12    | 20     |
      | 1     | +   | 1     | 2      |
      | 30    | -   | 10    | 20     |
      | 8     | -   | 5     | 3      |
      | 12    | -   | 6     | 6      |
      | 2     | -   | 1     | 1      |
      | 2     | x   | 8     | 16     |
      | 9     | x   | 5     | 45     |
      | 5     | x   | 8     | 40     |
      | 20    | x   | 5     | 100    |
      | 8     | /   | 2     | 4      |
      | 25    | /   | 5     | 5      |
      | 30    | /   | 5     | 6      |
      | 6     | /   | 3     | 2      |
