Given /^Num (\d+) operation "([^"]*)" num (\d+)$/ do |num1, ope, num2|
  @num1 = num1
  @ope = ope
  @num2 = num2
end

When /^Calculator is run$/ do
  @output = `ruby calculator.rb #{@num1} #{@ope} #{@num2}`
  raise('Command failed!') unless $?.success?
end

Then /^The output should be "([^"]*)"$/ do |expected_output|
  @output.should == expected_output
end
