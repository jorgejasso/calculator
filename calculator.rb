class Calculator
  attr_accessor :first_number, :operation, :second_number

  def initialize(first_number, operation, second_number)
    @first_number = first_number.to_i
    @operation = operation
    @second_number = second_number.to_i
  end

  def operate
    case operation
      when '+' then add
      when '-' then subtract  
      when 'x' then multiply
      when '/' then divide
    end
  end

  private 

  def add
    first_number + second_number
  end

  def subtract
    first_number - second_number
  end

  def multiply
    first_number * second_number
  end

  def divide
    first_number / second_number
  end
end

@cal = Calculator.new(ARGV[0], ARGV[1], ARGV[2])
print @cal.operate
